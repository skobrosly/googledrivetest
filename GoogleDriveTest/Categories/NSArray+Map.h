#import <Foundation/Foundation.h>

typedef id (^mapFunction)(id item);
typedef id (^mapWithIndexFunction)(NSInteger index, id item);
typedef NSArray * (^flatMapFunction)(id item);

@interface NSArray (Map)
-(NSArray *)map:(mapFunction)function;
-(NSArray *)mapWithIndex:(mapWithIndexFunction)function;
-(NSArray *)flatMap:(flatMapFunction)function;
@end