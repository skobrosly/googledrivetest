//
//  VCLogin.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCLogin.h"
#import "UIViewController+Init.h"
#import "GoogleAuthHandler.h"

@interface VCLogin ()
@property (nonatomic, strong) GoogleAuthHandler *auth;
@property (nonatomic, strong) IBOutlet UIButton *authButton;
@end

@implementation VCLogin
#pragma mark - Lifecycle
+(instancetype)loginWithAuth:(GoogleAuthHandler *)auth {
    VCLogin *instance = [VCLogin instance];
    instance.auth = auth;
    return instance;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.auth == nil) {
        [NSException
         raise:NSStringFromClass([self class])
         format:@"No auth object provided, please use +loginWithAuth: to create an instance"];
    }
    
    [self bindToAuth];
    [self updateUIForAuthState];
}
-(IBAction)loginPress:(id)sender {
    if ([self.auth isAuthenticated]) {
        [self.auth signOut];
    } else {
        [self.auth signIn];
    }
}

#pragma mark - Private - Events
-(void)didLogin {
    if (self.loginBlock) { self.loginBlock(); }
}

#pragma mark - Private - UI Updates
-(void)updateUIForAuthState {
    NSString *buttonTitle = ([self.auth isAuthenticated] ? @"Sign Out" : @"Sign In with Google");
    [self.authButton setTitle:buttonTitle forState:UIControlStateNormal];
}

#pragma mark - Private - Auth Events
-(void)bindToAuth {
    [self bindToAuthErrors];
    [self bindToAuthPresentVC];
    [self bindToAuthDismissVC];
    [self bindToAuthSignIn];
    [self bindToAuthSignOut];
}
-(void)bindToAuthErrors {
    self.auth.errorBlock = ^(NSError *error) {
        [[[UIAlertView alloc]
          initWithTitle:@"Oops.."
          message:error.localizedDescription
          delegate:nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil]
         show];
    };
}
-(void)bindToAuthPresentVC {
    __weak typeof(self) weakSelf = self; //the weak/strong dance
    self.auth.presentBlock = ^(UIViewController *vc) {
        __strong typeof(weakSelf) strongSelf = weakSelf; //stops memory leaks yet ensures the block doesn't lose its reference to self
        [strongSelf presentViewController:vc animated:YES completion:nil];
    };
}
-(void)bindToAuthDismissVC {
    self.auth.dismissBlock = ^(UIViewController *vc) {
        [vc dismissViewControllerAnimated:YES completion:nil];
    };
}
-(void)bindToAuthSignIn {
    __weak typeof(self) weakSelf = self;
    self.auth.signInBlock = ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf updateUIForAuthState];
        [strongSelf didLogin];
    };
}
-(void)bindToAuthSignOut {
    __weak typeof(self) weakSelf = self;
    self.auth.signOutBlock = ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf updateUIForAuthState];
    };
}
@end
