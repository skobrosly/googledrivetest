//
//  VCListTableViewCoordinator.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCListTableViewCoordinator : NSObject
-(void)reloadData:(NSArray *)items;
@end
