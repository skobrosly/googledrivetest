//
//  VCList.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCList.h"
#import "UIViewController+Init.h"
#import "VCListViewModel.h"
#import "VCListTableViewCoordinator.h"

@interface VCList ()
@property (nonatomic, strong) VCListViewModel *model;
@property (nonatomic, strong) IBOutlet VCListTableViewCoordinator *tableCoordinator;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *spinner;
@end

@implementation VCList
#pragma mark - Lifecycle
+(instancetype)listWithViewModel:(VCListViewModel *)model {
    VCList *instance = [VCList instance];
    instance.model = model;
    return instance;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.model == nil) {
        [NSException
         raise:NSStringFromClass([self class])
         format:@"No view model, please use +listWithViewModel: to create an instance"];
    }
    
    self.title = @"Your Google Drive Files";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleDone target:self action:@selector(logoutPress:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(refreshPress:)];
    
    [self bindToModel];
    [self.model refreshData];
}

#pragma mark - Actions
-(void)logoutPress:(id)sender {
    [self.model signOut];
}
-(void)refreshPress:(id)sender {
    [self.model refreshData];
}

#pragma mark - Model
-(void)bindToModel {
    [self bindToModelWorkingState];
    [self bindToModelError];
    [self bindToModelUpdate];
}
-(void)bindToModelWorkingState {
    __weak typeof(self) weakSelf = self;
    self.model.workingBlock = ^(BOOL working) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (working) {
            [strongSelf.spinner startAnimating];
        } else {
            [strongSelf.spinner stopAnimating];
        }
    };
}
-(void)bindToModelError {
    self.model.errorBlock = ^(NSError *error) {
        [[[UIAlertView alloc]
          initWithTitle:@"Oops.."
          message:error.localizedDescription
          delegate:nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil]
         show];
    };
}
-(void)bindToModelUpdate {
    __weak typeof(self) weakSelf = self;
    self.model.updateBlock = ^(NSArray *items) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.tableCoordinator reloadData:items];
    };
}
@end
