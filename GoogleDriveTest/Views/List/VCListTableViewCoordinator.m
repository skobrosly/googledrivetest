//
//  VCListTableViewCoordinator.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCListTableViewCoordinator.h"
#import "VCListDataSource.h"
#import "VCListDelegate.h"

@interface VCListTableViewCoordinator ()
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet VCListDataSource *dataSource;
@property (nonatomic, strong) IBOutlet VCListDelegate *delegate;
@end

@implementation VCListTableViewCoordinator
-(void)reloadData:(NSArray *)items {
    NSArray *sortedItems = [items sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    self.dataSource.items = sortedItems;
    self.delegate.items = sortedItems;
    [self.tableView reloadData];
}
@end
