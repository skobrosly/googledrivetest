//
//  VCListViewModel.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GoogleAuthHandler;

typedef void(^listWorking)(BOOL working);
typedef void(^listError)(NSError *error);
typedef void(^listUpdate)(NSArray *items);

@interface VCListViewModel : NSObject
-(instancetype)initWithAuth:(GoogleAuthHandler *)auth;

-(void)refreshData;
-(void)signOut;

@property (nonatomic, copy) listWorking workingBlock;
@property (nonatomic, copy) listError errorBlock;
@property (nonatomic, copy) listUpdate updateBlock;
@end
