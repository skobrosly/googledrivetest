//
//  VCListDataSource.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "VCListDataSource.h"
#import "UITableViewCell+Automation.h"

@implementation VCListDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (self.items != nil ? 1 : 0);
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [UITableViewCell instance:tableView nibName:NSStringFromClass([UITableViewCell class]) indexPath:indexPath];
}
@end
