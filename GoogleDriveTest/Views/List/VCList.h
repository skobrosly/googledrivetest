//
//  VCList.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VCListViewModel;

@interface VCList : UIViewController
+(instancetype)listWithViewModel:(VCListViewModel *)model;
@end
