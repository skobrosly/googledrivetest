//
//  AppDelegate.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "AppDelegate.h"
#import "GoogleAuthHandler.h"
#import "VCLogin.h"
#import "VCList+Factory.h"

@interface AppDelegate ()
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) GoogleAuthHandler *auth;
@end

@implementation AppDelegate
#pragma mark - Lifecycle
-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.auth = [GoogleAuthHandler new];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.navController = [UINavigationController new];
    [self navigateToRequiredViewController];
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

#pragma mark - Navigation Flow (this should be abstracted out in production..)
-(void)navigateToRequiredViewController {
    BOOL animated = (self.navController.viewControllers.count != 0);
    [self.navController setViewControllers:@[[self requiredViewController]] animated:animated];
}
-(UIViewController *)requiredViewController {
    return ([self.auth isAuthenticated] ? [self listViewController] : [self loginViewController]);
}
-(VCLogin *)loginViewController {
    VCLogin *instance = [VCLogin loginWithAuth:self.auth];
    [self bindToLogin:instance]; //NOTE: side-effect, don't really like this here, but for a non-production app it will do for now
    return instance;
}
-(VCList *)listViewController {
    VCList *instance = [VCList factoryInstance];
    [self bindToLogout]; //NOTE: side-effect, don't really like this here, but for a non-production app it will do for now
    return instance;
}

#pragma mark - Auth Events
-(void)bindToLogin:(VCLogin *)login {
    login.loginBlock = ^{
        [self navigateToRequiredViewController];
    };
}
-(void)bindToLogout {
    __weak typeof(self) weakSelf = self;
    self.auth.signOutBlock = ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf navigateToRequiredViewController];
    };
}
@end
