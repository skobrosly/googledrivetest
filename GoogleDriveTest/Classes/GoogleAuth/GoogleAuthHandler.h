//
//  GoogleAuthHandler.h
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google-API-Client/GTLDrive.h>

typedef void(^googleAuthPresentAuthController)(UIViewController *vc);
typedef void(^googleAuthDismissAuthController)(UIViewController *vc);
typedef void(^googleAuthDidSignIn)();
typedef void(^googleAuthDidSignOut)();
typedef void(^googleAuthDidError)(NSError *error);

@interface GoogleAuthHandler : NSObject
-(void)signIn;
-(void)signOut;
-(BOOL)isAuthenticated;

@property (nonatomic, copy) googleAuthPresentAuthController presentBlock;
@property (nonatomic, copy) googleAuthPresentAuthController dismissBlock;
@property (nonatomic, copy) googleAuthDidSignIn signInBlock;
@property (nonatomic, copy) googleAuthDidSignOut signOutBlock;
@property (nonatomic, copy) googleAuthDidError errorBlock;

//NOTE: in production google auth would not create/be responsible for the drive server directly. placed here for ease
@property (nonatomic, retain, readonly) GTLServiceDrive *googleDrive;
@end
