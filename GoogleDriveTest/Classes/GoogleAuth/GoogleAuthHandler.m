//
//  GoogleAuthHandler.m
//  GoogleDriveTest
//
//  Created by Ian Keen on 22/05/2015.
//  Copyright (c) 2015 Mustard. All rights reserved.
//

#import "GoogleAuthHandler.h"
#import <gtm-oauth2/GTMOAuth2ViewControllerTouch.h>

static NSString *const kKeychainItemName = @"GoogleDriveTest";
static NSString *const kClientID = @"283664343569-tho2md8doto24riv8i4bimonb0ijjksd.apps.googleusercontent.com";
static NSString *const kClientSecret = @"TwZucanlgzWrobORhTUN8Y2-";

@implementation GoogleAuthHandler
#pragma mark - Lifecycle
-(instancetype)init {
    if (!(self = [super init])) { return nil; }
    _googleDrive = [self newDriveService];
    
    GTMOAuth2Authentication *auth = [self newAuthentication];
    if ([auth canAuthorize]) {
        [self didSignIn:auth];
    }
    
    return self;
}

#pragma mark - Public
-(void)signIn {
    GTMOAuth2Authentication *auth = [self newAuthentication];
    if (![auth canAuthorize]) {
        [self presentSignInViewController];
        
    } else {
        [self didSignIn:auth];
    }
}
-(void)signOut {
    [self didSignOut];
}
-(BOOL)isAuthenticated {
    return [[self newAuthentication] canAuthorize];
}

#pragma mark - Private - Events
-(void)didSignIn:(GTMOAuth2Authentication *)auth {
    self.googleDrive.authorizer = auth;
    
    if (self.signInBlock) { self.signInBlock(); }
}
-(void)didSignOut {
    self.googleDrive.authorizer = nil;
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
    
    if (self.signOutBlock) { self.signOutBlock(); }
}
-(void)didNeedSignInPresentation:(UIViewController *)vc {
    if (self.presentBlock) { self.presentBlock(vc); }
}
-(void)didNeedSignInDismissal:(UIViewController *)vc {
    if (self.dismissBlock) { self.dismissBlock(vc); }
}
-(void)didError:(NSError *)error {
    if (self.errorBlock) { self.errorBlock(error); }
}

#pragma mark - Private - Object Creation
-(GTLServiceDrive *)newDriveService {
    GTLServiceDrive *service = [GTLServiceDrive new];
    service.shouldFetchNextPages = YES;
    service.retryEnabled = YES;
    return service;
}
-(GTMOAuth2Authentication *)newAuthentication {
    GTMOAuth2Authentication *auth =
    [GTMOAuth2ViewControllerTouch
     authForGoogleFromKeychainForName:kKeychainItemName
     clientID:kClientID
     clientSecret:kClientSecret];
    
    return auth;
}

#pragma mark - Private - SignIn Flow
-(void)presentSignInViewController {
    GTMOAuth2ViewControllerTouch *authViewController =
    [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeDriveReadonly
                                               clientID:kClientID
                                           clientSecret:kClientSecret
                                       keychainItemName:kKeychainItemName
                                               delegate:self
                                       finishedSelector:@selector(didComplete:auth:error:)];
    
    [self didNeedSignInPresentation:authViewController];
}
-(void)didComplete:(GTMOAuth2ViewControllerTouch *)authViewController auth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    [self didNeedSignInDismissal:authViewController];
    
    if (error) {
        [self didError:error];
    } else {
        [self didSignIn:auth];
    }
}
@end
